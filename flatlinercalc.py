import PySimpleGUI as sg
from math import ceil
from datetime import datetime
from os import path
from ortools.linear_solver import pywraplp

def ParseFile(Path):
    sect = []
    try:
        with open(Path, "r") as f:        
            lines = f.readlines()
            for line in lines:
                sect.append(int(ceil(float(line))))
    except:
        return -1
    return sect

def report(Path, sect, sol, waste, algorithm):
    string = ''
    string += 'Flatliner Reel Calculator Report'
    if algorithm == 'SCIP':
        string += 'Using SCIP algorithm from:\nhttps://developers.google.com/optimization/bin/bin_packing\nand\nhttps://www.scipopt.org/\n\n'
    else:
        string += 'Using FFD algorithm from:\nhttps://en.wikipedia.org/wiki/Bin_packing_problem#First_Fit_Decreasing_(FFD)\n\n'
    string += 'Total Section Length: %d m\n\n' % sum(sect)
    string += 'Standard Reel Length: %s m\n' % values['-RL-']
    string += 'Safety Margin %s percent!\n' % values['-SM-']
    string += 'Extra length per section %s m\n\n' % values['-EL-']
    string += 'Total Length used for calculation %d m\n\n' % sum(newsect)
    string += 'Number of Reels %d\n' % len(sol)
    string += 'Total wasted length %d\n' % sum(waste)
    counter = 1
    for i in sol:
        string += '\nReel nr. %d (Wasted: %d m)\n|' % (counter,waste[counter-1])
        for j in i:
            string += '%d|' % j
        string += '\n'
        counter += 1
    try:
        with open(Path, "w") as f:        
            f.writelines(string)
    except:
        return -1
    return 0

def SCIP(sect, D):
    # based on: https://developers.google.com/optimization/bin/bin_packing
    solver = pywraplp.Solver.CreateSolver('SCIP')
    
    # Variables
    # x[i, j] will hold all the combinations of sections i being assigned to reel j
    x = {}
    for i in range(len(sect)):
        for j in range(len(sect)):
            x[(i, j)] = solver.IntVar(0, 1, 'x_%i_%i' % (i, j))

    # y[j] = 1 if reel j is used (optimization starts with as many reels as sections)
    y = {}
    for j in range(len(sect)):
        y[j] = solver.IntVar(0, 1, 'y[%i]' % j)
    
    # Constraints
    # Each section can only be assigned to exactly one reel
    for i in range(len(sect)):
        solver.Add(sum(x[i, j] for j in range(len(sect))) == 1)

    # The number of sections assigned to a reel cannot exceed its length
    for j in range(len(sect)):
        solver.Add(sum(x[(i, j)] * sect[i] for i in range(len(sect))) <= y[j] * D)

    # Objective: minimize the number of reels used
    solver.Minimize(solver.Sum([y[j] for j in range(len(sect))]))

    waste = []  # waste per reel
    sol = []  # final solution

    status = solver.Solve()
    if status == pywraplp.Solver.OPTIMAL:
        num_bins = 0
        for j in range(len(sect)):
            if y[j].solution_value() == 1:
                bin_items = []
                bin_weight = 0
                sol.append([])
                for i in range(len(sect)):
                    if x[i, j].solution_value() > 0:
                        bin_items.append(i)
                        bin_weight += sect[i]
                        sol[-1].append(sect[i])
                if bin_weight > 0:
                    num_bins += 1                   
                    waste.append(D-bin_weight)
    else:
        return [],[]    
    return sol, waste

def FFD(sect, D):
    waste = []  # waste per reel
    sol = []  # final solution

    for i in sect:    
        for j in range(len(waste)):  # cycle through the remaining reel lengths
            if i <= waste[j]:  # check if a section fits in the current considered reel
                sol[j].append(i)  # cut section from the reel
                waste[j] -= i  # update remaining length for this reel                
                break
        else: #if added == 0:
            sol.append([i])  # cut section from the new reel
            waste.append(D-i)  # update remaining length on the new reel
    return sol, waste

######################################################################################################
######################################################################################################
######################################################################################################
menu = [['File', ['Open', 'Save Report', ['FFD Report', 'SCIP Report'], 'Exit']],            
        ['Help', 'About...']]  

tloRL='standard reel size'
tloSM='extra percentage of length to add due to documentation uncertainty'
tloEL='extra metres of length to add to each section (for example extra length inside a shaft)'
layout_options = sg.Frame(layout=[      
          [sg.Text('Reel Length[m]', tooltip=tloRL), sg.InputText('300',key='-RL-',size=(5,1),justification='r', tooltip=tloRL)],
          [sg.Text('Safety Margin[%]', tooltip=tloSM), sg.InputText('5',key='-SM-',size=(3,1),justification='r', tooltip=tloSM)],
          [sg.Text('Extra length per section[m]', tooltip=tloEL), sg.InputText('3',key='-EL-',size=(2,1),justification='r', tooltip=tloEL),], 
          [sg.Text('')],
          ],title='Options',relief=sg.RELIEF_SUNKEN)

layout_sections = sg.Frame(layout=[      
          [sg.Text('Total length:'), sg.Text('',key='-TL-',size=(8,1))],
          [sg.Text('Number of Sections:'), sg.Text('',key='-NR-',size=(8,1))],
          [sg.Text('Largest Section:'), sg.Text('',key='-MAXS-',size=(8,1))],
          [sg.Text('Smallest Section:'), sg.Text('',key='-MINS-',size=(8,1))],
          ],title='Sections Information',relief=sg.RELIEF_SUNKEN)

layout_results_FFD = sg.Frame(layout=[    
          [sg.Text('Total length:'), sg.Text('',key='-RFFDTL-',size=(8,1))],  
          [sg.Text('Number of Reels:'), sg.Text('',key='-RFFDNR-',size=(12,1))],
          [sg.Text('Wasted Length:'), sg.Text('',key='-RFFDW-',size=(12,1))],          
          [sg.Text('Execution Time:'), sg.Text('',key='-RFFDT-',size=(14,1))]
          ],title='Results FFD',relief=sg.RELIEF_SUNKEN)

layout_results_SCIP = sg.Frame(layout=[    
          [sg.Text('Total length:'), sg.Text('',key='-RSCIPTL-',size=(8,1))],  
          [sg.Text('Number of Reels:'), sg.Text('',key='-RSCIPNR-',size=(12,1))],
          [sg.Text('Wasted Length:'), sg.Text('',key='-RSCIPW-',size=(12,1))],          
          [sg.Text('Execution Time:'), sg.Text('',key='-RSCIPT-',size=(14,1))]          
          ],title='Results SCIP',relief=sg.RELIEF_SUNKEN)

text_FileInfo = 'Please provide a "Sections" file through File>Open'

layout = [[sg.Menu(menu, tearoff=True)],
          [sg.Text(text_FileInfo,key='-FileInfo-', size=(80,1), text_color='darkred')],
          [layout_options,layout_sections, layout_results_FFD, layout_results_SCIP],          
          [sg.Button('Load File'), sg.Button('Calculate', disabled=True)]]

# Create the window
window = sg.Window("Flatliner Reel Calculator", layout)

# State Variables
file_loaded = 0
calculation_performed = 0

# Create an event loop
while True:
    event, values = window.read()

    if event == 'FFD Report' or event == 'SCIP Report':
        if calculation_performed == 0:
            sg.popup('Error', 'Please perform a calculation first')          
        elif event == 'SCIP Report':
            if calculation_performed == 1:
                sg.popup('Error', 'SCIP calculation failed, report cannot be generated')
            else:
                filepath = sg.popup_get_file('Document to save', save_as=True)
                if filepath is not None:
                    result = report(filepath, sect, sol2, waste2, 'SCIP')
                    if result == -1:
                        sg.popup('Error', 'Problem saving file')
        else:
            filepath = sg.popup_get_file('Document to save', save_as=True)
            if filepath is not None:
                result = report(filepath, sect, sol, waste, 'FFD')
                if result == -1:
                    sg.popup('Error', 'Problem saving file')

    if event == 'About...':
        sg.popup('About', 'Flatliner Reel Calculator\nVersion: 0.01')
    if event == 'Open' or event == 'Load File':
        filepath = sg.popup_get_file('Document to open')
        #clear output values        
        window['-TL-'].update('')
        window['-NR-'].update('')
        window['-MAXS-'].update('')
        window['-MINS-'].update('')
        window['-RFFDNR-'].update('')
        window['-RFFDW-'].update('')
        window['-RFFDTL-'].update('')
        window['-RFFDT-'].update('')
        window['-RSCIPNR-'].update('')
        window['-RSCIPW-'].update('')
        window['-RSCIPTL-'].update('')
        window['-RSCIPT-'].update('')        
        window['Calculate'].update(disabled=True)
        file_loaded = 0
        calculation_performed = 0

        sect = ParseFile(filepath)  # parses the file and returns a list or -1 in case of error
        if filepath == '':
            sg.popup('Error', 'No filename supplied')
            window['-FileInfo-'].update(text_FileInfo, text_color='darkred')       
        elif not isinstance(sect, list):
            sg.popup('Error', 'Could not load "Sections" file, one length per line in metres is required')
            window['-FileInfo-'].update(text_FileInfo, text_color='darkred')
            #window['-FileError-'].update('Could not load "Sections" file, one length per line in metres is required') 
        else:            
            window['-FileInfo-'].update(filepath, text_color='white')
            window['-TL-'].update(sum(sect))
            window['-NR-'].update(len(sect))
            window['-MAXS-'].update(max(sect))
            window['-MINS-'].update(min(sect))
            window['Calculate'].update(disabled=False)
            file_loaded = 1
    
    if event == 'Exit' or event == sg.WIN_CLOSED:
        break
    if event == "OK" or event == sg.WIN_CLOSED:
        break
    
    if event == 'Calculate':        
        if file_loaded != 1:
            sg.popup('Error', 'Please load a "Sections" file')
            #window['-CALTEXT-'].update('Please Load a File first') 
        else:
            newsect = []
            SM = 1+float(values['-SM-'])/100
            EL = float(values['-EL-'])          
            for i in sect:
                newsect.append(ceil(i*SM+EL))
            window['-RFFDTL-'].update(sum(newsect))
            window['-RSCIPTL-'].update(sum(newsect))
            RL = int(values['-RL-'])

            if max(newsect) > RL:
                sg.popup('Error', 'Loaded file contains sections larger than the reel length (after applying the safety margin and extra length)')

            else:
                start = datetime.now()
                sol, waste = FFD(newsect, RL)
                stop = datetime.now()
                window['-RFFDT-'].update(stop-start)

                start = datetime.now()
                sol2, waste2 = SCIP(newsect, RL)
                stop = datetime.now()
                window['-RSCIPT-'].update(stop-start)
                
                window['-RFFDNR-'].update(len(sol))
                window['-RFFDW-'].update(sum(waste))
                
                if len(sol2) == 0:  # no solution found
                    window['-RSCIPNR-'].update('no solution')
                    window['-RSCIPW-'].update('no solution')
                    calculation_performed = 1
                else:
                    window['-RSCIPNR-'].update(len(sol2))
                    window['-RSCIPW-'].update(sum(waste2))
                    calculation_performed = 2
                
window.close()


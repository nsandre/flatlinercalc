# Flatliner Reel Calculator

Simple (quick and dirty) implementation of the First Fit Decreasing and SCIP (mixed-integer programming) algorithms applied to the minimization of Flatliner Reel use in the installation of conduits for passive optical networks.

The optimization/heuristic is based on the bin packing problem.

FDD:

https://en.wikipedia.org/wiki/Bin_packing_problem#First_Fit_Decreasing_(FFD)

SCIP:

https://developers.google.com/optimization/bin/bin_packing

https://www.scipopt.org/

## Requirements for the .exe creation in Windows
* Microsoft Visual C++ Redistributable for Visual Studio 2019
* run `pyinstaller -wF flatlinercalc.py`
